/*
 * Public API Surface of ifa-libs
 */

export * from './lib/ifa-libs.service';
export * from './lib/ifa-libs.component';
export * from './lib/ifa-libs.module';
