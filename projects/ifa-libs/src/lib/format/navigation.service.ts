import { EventEmitter, Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { INavigate, PageEnum } from './main/main.component';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  public routeBased = true;

  constructor(private router: Router) { }


  public performNavigation(page: PageEnum, emitter: EventEmitter<INavigate>) {
    if (this.routeBased) {
      this.router.navigate(['/', PageEnum[page]]);
      //this.router.navigateByUrl('/' + PageEnum[page]);
    } else {
      emitter.emit({ target: page });
    }
  }

}
