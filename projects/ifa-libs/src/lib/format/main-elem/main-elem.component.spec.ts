import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainElemComponent } from './main-elem.component';

describe('MainElemComponent', () => {
  let component: MainElemComponent;
  let fixture: ComponentFixture<MainElemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainElemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainElemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
