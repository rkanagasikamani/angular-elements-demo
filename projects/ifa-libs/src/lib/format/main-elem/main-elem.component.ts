import { Component, NgModule, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageEnum, INavigate } from '../main/main.component';

@Component({
  templateUrl: './main-elem.component.html',
  styleUrls: ['./main-elem.component.scss']
})
export class MainElemComponent implements OnInit {

  public comp: PageEnum;
  public allPages = PageEnum;
  public routeBased = true;


  constructor(private router: Router) {
    this.comp = PageEnum.summary;
    this.router.events.subscribe(console.log);
  }

  ngOnInit(): void {
  }

  onNavigation(sourceComponent: string, event: INavigate) {
    console.log('onNavigation - ', sourceComponent, '; event - ', event);
    this.comp = event.target;
  }
}