import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NavigationService } from '../../navigation.service';
import { INavigate, PageEnum } from '../main.component';

@Component({
  selector: 'lib-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  @Output("navigate") navigate: EventEmitter<INavigate> = new EventEmitter();

  constructor(private navService: NavigationService) { }

  ngOnInit(): void {
  }

  review(): void {
    this.navService.performNavigation(PageEnum.review, this.navigate);
  }

  backToSummary(): void {
    this.navService.performNavigation(PageEnum.summary, this.navigate);

  }
}
