import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { INavigate, PageEnum } from '../main.component';
import { NavigationService } from '../../navigation.service';

@Component({
  selector: 'lib-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {


  @Output("navigate") navigate: EventEmitter<INavigate> = new EventEmitter();

  constructor(private navService: NavigationService) {

  }

  ngOnInit(): void {
  }

  create() {
    this.performNavigation(PageEnum.create);
  }

  edit() {
    this.performNavigation(PageEnum.edit);
  }

  details() {
    this.performNavigation(PageEnum.details);
  }

  private performNavigation(page: PageEnum) {
    this.navService.performNavigation(page, this.navigate);
  }

}
