import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NavigationService } from '../../navigation.service';
import { INavigate, PageEnum } from '../main.component';

@Component({
  selector: 'lib-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  @Output("navigate") navigate: EventEmitter<INavigate> = new EventEmitter();

  constructor(private navService: NavigationService) { }

  ngOnInit(): void {
  }

  edit(): void {
    this.navService.performNavigation(PageEnum.edit, this.navigate);
  }

  backToSummary(): void {
    this.navService.performNavigation(PageEnum.summary, this.navigate);

  }

}
