import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { INavigate, PageEnum } from '../main.component';
import { NavigationService } from '../../navigation.service';

@Component({
  selector: 'lib-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  @Output("navigate") navigate: EventEmitter<INavigate> = new EventEmitter();

  constructor(private navService: NavigationService) { }

  ngOnInit(): void {
  }

  backToSummary() {
    this.navService.performNavigation(PageEnum.summary, this.navigate);
  }

  review() {
    this.navService.performNavigation(PageEnum.review, this.navigate);
  }

  
}
