import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../navigation.service';

@Component({
  selector: 'lib-format-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public comp: PageEnum;
  public allPages = PageEnum;
  public routeBased = false;


  constructor() {
    this.comp = PageEnum.summary;
  }

  ngOnInit(): void {
  }

  onNavigation(sourceComponent: string, event: INavigate) {
    console.log('onNavigation - ', sourceComponent, '; event - ', event);
    this.comp = event.target;
  }
}

export interface INavigate {
  target: PageEnum;
}

export enum PageEnum {
  summary, details, edit, review, create
}