import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { INavigate, PageEnum } from '../main.component';
import { NavigationService } from '../../navigation.service';

@Component({
  selector: 'lib-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {

  @Output("navigate") navigate: EventEmitter<INavigate> = new EventEmitter();

  constructor(private navService: NavigationService) { }

  ngOnInit(): void {
  }


  edit(): void {
    this.navService.performNavigation(PageEnum.edit, this.navigate);
  }

  submit(): void {
    this.navService.performNavigation(PageEnum.summary, this.navigate);
  }

}
