import { CUSTOM_ELEMENTS_SCHEMA, NgModule, Injector } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { SummaryComponent } from './main/summary/summary.component';
import { EditComponent } from './main/edit/edit.component';
import { CreateComponent } from './main/create/create.component';
import { DetailsComponent } from './main/details/details.component';
import { ReviewComponent } from './main/review/review.component';
import { SharedModule } from '../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { MainElemComponent } from './main-elem/main-elem.component';
import { createCustomElement } from '@angular/elements';


const routes: Routes = [
  { path: 'summary', component: SummaryComponent },
  { path: 'edit', component: EditComponent },
  { path: 'review', component: ReviewComponent },
  { path: 'details', component: DetailsComponent },
  { path: 'create', component: CreateComponent },

];


@NgModule({
  declarations: [MainComponent, SummaryComponent, EditComponent, CreateComponent, DetailsComponent, ReviewComponent, MainElemComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule, SharedModule
  ],
  exports: [
    MainComponent, MainElemComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FormatModule {

  constructor(private injector: Injector) {
    const mainElemComponent = createCustomElement(MainElemComponent, { injector });
    customElements.define('main-elem-component', mainElemComponent);

  }
}
