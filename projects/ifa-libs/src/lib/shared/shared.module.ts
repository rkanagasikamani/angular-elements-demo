import { NgModule } from '@angular/core';
import { FilterComponent } from './filter/filter.component';
import { MatCommonModule } from './mat-common/mat-common.module';



@NgModule({
  declarations: [FilterComponent],
  imports: [
    MatCommonModule
  ],
  exports: [
    MatCommonModule
  ]
})
export class SharedModule { }
