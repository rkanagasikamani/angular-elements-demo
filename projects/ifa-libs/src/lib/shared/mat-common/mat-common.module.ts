import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';

let MATERIAL_MODULES = [
  MatButtonModule
]

@NgModule({
  declarations: [],
  imports: [
    ...MATERIAL_MODULES
  ],
  exports: [
    ...MATERIAL_MODULES
  ]
})
export class MatCommonModule { }
