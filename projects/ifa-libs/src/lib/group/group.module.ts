import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { GroupSummaryComponent } from './main/group-summary/group-summary.component';
import { GroupDetailsComponent } from './main/group-details/group-details.component';



@NgModule({
  declarations: [MainComponent, GroupSummaryComponent, GroupDetailsComponent],
  imports: [
    CommonModule
  ],
  exports: [
    MainComponent
  ]
})
export class GroupModule { }
