import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatModule } from '../format/format.module';
import { GroupModule } from '../group/group.module';
import { SharedModule } from '../shared/shared.module';
import { IfaRootComponent } from './ifa-root/ifa-root.component';



@NgModule({
  declarations: [IfaRootComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormatModule,
    GroupModule
  ],
  exports: [IfaRootComponent]
})
export class IfaRootModule { }
