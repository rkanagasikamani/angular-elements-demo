import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IfaRootComponent } from './ifa-root.component';

describe('IfaRootComponent', () => {
  let component: IfaRootComponent;
  let fixture: ComponentFixture<IfaRootComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IfaRootComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IfaRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
