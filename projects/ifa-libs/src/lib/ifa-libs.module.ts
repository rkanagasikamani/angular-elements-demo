import { NgModule } from '@angular/core';
import { FormatModule } from './format/format.module';
import { GroupModule } from './group/group.module';
import { IfaLibsComponent } from './ifa-libs.component';
import { IfaRootModule } from './ifa-root/ifa-root.module';
import { IfaRootComponent } from './ifa-root/ifa-root/ifa-root.component';
import { SharedModule } from './shared/shared.module';



@NgModule({
  declarations: [IfaLibsComponent],
  imports: [
    FormatModule,
    SharedModule
  ],
  exports: [IfaLibsComponent]
})
export class IfaLibsModule { }
