import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IfaLibsComponent } from './ifa-libs.component';

describe('IfaLibsComponent', () => {
  let component: IfaLibsComponent;
  let fixture: ComponentFixture<IfaLibsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IfaLibsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IfaLibsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
