import { TestBed } from '@angular/core/testing';

import { IfaLibsService } from './ifa-libs.service';

describe('IfaLibsService', () => {
  let service: IfaLibsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IfaLibsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
