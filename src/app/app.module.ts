import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IfaLibsModule } from 'projects/ifa-libs/src/public-api';
import { SharedModule } from 'projects/ifa-libs/src/lib/shared/shared.module';
import { FormatModule } from '../../projects/ifa-libs/src/lib/format/format.module';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';





@NgModule({
  declarations: [
    AppComponent,
    Page1Component,
    Page2Component
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    FormatModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor() {
  }
}
